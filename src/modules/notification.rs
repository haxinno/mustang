use chrono::{NaiveDateTime, Utc};
use fcm::{Client, MessageBuilder, NotificationBuilder};
use mysql::{Pool, PooledConn, Row, params, prelude::Queryable};
use uuid::Uuid;

use crate::entities::notification::Notification;

impl Notification {

    ///
    /// Used to create a new notification
    ///
    pub fn new(user: String, token: String, platform: String) -> Self {
        Self {
            date: Utc::now().naive_utc(),
            platform: platform.to_string(),
            token: token.to_string(),
            uuid: Uuid::new_v4().to_string(),
            user: user.to_string()
        }
    }

    ///
    /// Exist on the database
    ///
    async fn exist(mut conn: PooledConn, user: String, token: String) -> bool {
        let mut rows: Vec<Notification> = Vec::new();
        conn.exec_iter("SELECT * FROM notification WHERE user = :user AND token = :token", params!{
            "user" => &user,
            "token" => &token
        }).map(|mut result| {
            loop {
                let r = result.next();
                
                match r {
                    Some(e) => {
                        match e {
                            Ok(row) => {
                                rows.push(Notification::convert_from_mysql(row.clone()))
                            },
                            Err(e) => {
                                println!("Pb sql: {:?}", e);
                                return true
                            }
                        }
                    },
                    None => return false
                }
    
            }
        }).err();

        if rows.len() > 0 {
            return true
        } else {
            return false
        }
    }

    ///
    /// Save the notification
    ///
    pub async fn save(&self, rds: &Pool) -> bool {
        if Notification::exist(rds.get_conn().unwrap(), self.user.to_string(), self.token.to_string()).await {
            println!("Already exist");
            return false;
        } else {
            let res = rds.get_conn().unwrap().exec_drop("INSERT INTO notification (uuid, date, user, token, platform) VALUES (:uuid, :date, :user, :token, :platform)", params!{
                "uuid" => self.uuid.to_string(),
                "platform" => self.platform.to_string(),
                "token" => self.token.to_string(),
                "user" => self.user.to_string(),
                "date" => &self.date.format("%Y-%m-%d %H:%M:%S").to_string(),
            });
            
            match res {
                Ok(_e) => {},
                Err(e) => println!("{:?}", e)
            }
        }

        return true;
    }

    ///
    /// Send a notification to every user
    ///
    pub async fn sends_to_all(conn: PooledConn, title: &String, message: &String) -> bool {
        let client = Client::new();

        let mut tokens = Vec::new();
        let notifs = Notification::all(conn).await;

        for n in notifs {
            tokens.push(n.token)
        }

        let mut notification_builder = NotificationBuilder::new();
        notification_builder.title(title);
        notification_builder.body(message);
        
        let notification = notification_builder.finalize();
        let mut message_builder = MessageBuilder::new_multi("AAAAUitXrMk:APA91bEyFIxzvAT-udEInMhqjvB_2bD3V70pL0_znHCm78UiGd3O293TDtB7aHoi4htJbM_X45Oeo5ZibcQnp1PSunR4p7cAX1CgdRgRlMusa9KCCGcRAVZIYYWanKOV5HCZr-fDA3iw", &tokens);
        message_builder.notification(notification);
        
        let response = client.send(message_builder.finalize()).await;

        match response {
            Ok(e) => println!("{:?}", e),
            Err(e) => println!("{:?}", e)
        }

        return true
    }

    ///
    /// All notification 
    ///
    pub async fn all(mut conn: PooledConn) -> Vec<Notification> {
        let mut notifs: Vec<Notification> = Vec::new();
        conn.query_iter("SELECT * FROM notification").map(|mut result| {
            loop {
                let r = result.next();
                
                match r {
                    Some(e) => {
                        match e {
                            Ok(row) => {
                                notifs.push(Notification::convert_from_mysql(row.clone()))
                            },
                            Err(_err) => break
                        }
                    },
                    None => break
                }
    
            }
        }).err();

        return notifs;
    }

    ///
    /// Convert from mysql row to entity
    ///
    fn convert_from_mysql(mut row: Row) -> Notification {
        Notification {
            date: row.take::<NaiveDateTime,_>("date").unwrap(),
            token: row.take::<String,_>("token").unwrap(),
            platform: row.take::<String,_>("platform").unwrap(),
            user: row.take::<String,_>("user").unwrap(),
            uuid: row.take::<String,_>("uuid").unwrap(),
        }
    }

}
