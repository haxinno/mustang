use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct Notification {
    pub token: String,
    pub platform: String,
    pub user: String,
    pub date: NaiveDateTime,
    pub uuid: String,
}

///
/// Form for a notification save/create/update
///
#[derive(Debug, Deserialize, Serialize)]
pub struct NotificationForm {
    pub token: String,
    pub platform: String,
}
