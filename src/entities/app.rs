use std::env;
use mysql::Pool;
use rusoto_core::Region;
use rusoto_dynamodb::{DynamoDbClient};

pub struct App {
    pub db: DynamoDbClient,
    pub rds: Pool,
}

impl App {

    ///
    /// Init aws and mysql if needed
    ///
    pub fn init(aws_key_id: &str, aws_access_key: &str, mysql_url: &str, discord_url: &str) -> Self {
        env::set_var("AWS_ACCESS_KEY_ID", aws_key_id);
        env::set_var("AWS_SECRET_ACCESS_KEY", aws_access_key);
        env::set_var("DISCORD_URL", discord_url);
        
        let url = mysql_url;

        Self {
            db: DynamoDbClient::new(Region::EuWest2),
            rds: Pool::new(url).unwrap(),
        }
    }
}