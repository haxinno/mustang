///
/// Token auth from JWT
///
pub struct Token {
    pub user: String,
    pub account: String
}
