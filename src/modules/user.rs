use std::collections::HashMap;

use rusoto_dynamodb::AttributeValue;
use uuid::Uuid;

use crate::entities::user::User;


impl User {

    ///
    /// Used to create a new user
    ///
    pub fn new() -> Self {
        Self {
            account: "".to_string(),
            admin: false,
            mail: "".to_string(),
            password: "".to_string(),
            token: "".to_string(),
            uuid: Uuid::new_v4().to_string(),
        }
    }

    ///
    /// Convert from dynamo result hashmap to entity
    ///
    pub fn get_from_dynamo(hash: HashMap<String, AttributeValue>) -> Self {
        let mut tmp = User::new();

        // On parcours le hashmap de l'item
        for (key, attribute) in hash {
            match &key as &str {
                "account" => {
                    match attribute.s {
                        Some(x) => tmp.account = x,
                        None => {}
                    }
                    
                },
                "admin" => {
                    match attribute.bool {
                        Some(x) => tmp.admin = x,
                        None => {}
                    }
                    
                },
                "mail" => {
                    match attribute.s {
                        Some(x) => tmp.mail = x,
                        None => {}
                    }
                    
                },
                "password" => {
                    match attribute.s {
                        Some(x) => tmp.password = x,
                        None => {}
                    }
                    
                },
                "token" => {
                    match attribute.s {
                        Some(x) => tmp.token = x,
                        None => {}
                    }
                    
                },
                "uuid" => {
                    match attribute.s {
                        Some(x) => tmp.uuid = x,
                        None => {}
                    }
                    
                },
                _ => {}
            }
        }

        tmp
    }

}
