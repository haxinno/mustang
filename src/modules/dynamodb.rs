use std::collections::HashMap;
use rusoto_dynamodb::{AttributeValue};

///
/// Use in dynamodb for value
///
pub fn db_string (key: String, value: String) -> HashMap<String, AttributeValue> {
    let mut hash = HashMap::new();
    hash.insert(key, AttributeValue {
        s: Some(value),
        ..AttributeValue::default()
    });
    hash
}

///
/// Use in dynamodb for name
///
pub fn hashmap_string (key: String, value: String) -> HashMap<String, String> {
    let mut hash = HashMap::new();
    hash.insert(key, value);
    hash
}
