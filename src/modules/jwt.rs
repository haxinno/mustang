use jsonwebtoken::{Algorithm, DecodingKey, EncodingKey, Header, Validation, decode, encode};
use crate::entities::{claims::Claims, token::Token};

///
/// Generate jwt
///
pub fn generate(uuid: String, admin: bool) -> String {
    let my_claims = Claims {
        admin,
        exp: 10000000000,
        user: "".to_string(),
        account: uuid.to_string()
    };

    match encode(&Header::new(Algorithm::HS512), &my_claims, &EncodingKey::from_secret(include_bytes!("../key/old_key.key"))) {
        Ok(t) => t,
        Err(e) => format!("error parsing header: {:?}", e)
    }
}

///
/// Test if the user is a admin
///
pub fn is_admin(token: String) -> Option<Token> {
    match decode::<Claims>(
        &token,
        &DecodingKey::from_secret(include_bytes!("../key/old_key.key")),
        &Validation::new(Algorithm::HS512),
    ) {
        Ok(c) => {
            Some(Token {
                account: "".to_string(),
                user: c.claims.user
            })
        },
        Err(e) => {
            println!("error parsing header: {:?}", e);
            None
        }
    }
}

///
/// Test if the user is a user
///
pub fn is_user(token: String) -> Option<Token> {
    match decode::<Claims>(
        &token,
        &DecodingKey::from_secret(include_bytes!("../key/old_key.key")),
        &Validation::new(Algorithm::HS512),
    ) {
        Ok(c) => {
            Some(Token {
                account: "".to_string(),
                user: c.claims.user
            })
        },
        Err(e) => {
            println!("error parsing header: {:?}", e);
            None
        }
    }
}

///
/// Test if the account is a user
///
pub fn is_account(token: String) -> Option<Token> {
    match decode::<Claims>(
        &token,
        &DecodingKey::from_secret(include_bytes!("../key/old_key.key")),
        &Validation::new(Algorithm::HS512),
    ) {
        Ok(c) => {
            Some(Token {
                account: c.claims.account,
                user: "".to_string()
            })
        },
        Err(e) => {
            println!("error parsing header: {:?}", e);
            None
        }
    }
}

///
/// @TODO: Test if the account is a user
///
pub fn refresh(token: String) -> Option<Token> {
    match decode::<Claims>(
        &token,
        &DecodingKey::from_secret(include_bytes!("../key/old_key.key")),
        &Validation::new(Algorithm::HS512),
    ) {
        Ok(c) => {
            Some(Token {
                account: c.claims.account,
                user: "".to_string()
            })
        },
        Err(e) => {
            println!("error parsing header: {:?}", e);
            None
        }
    }
}
