use serde::{Deserialize, Serialize};

///
/// 
///
#[derive(Debug, Deserialize, Serialize)]
pub struct ResponseMultiple<T> {
    pub results: Option<Vec<T>>,
    pub status: String
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ResponseSimple<T> {
    pub results: Option<T>,
    pub status: String
}
