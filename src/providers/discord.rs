use std::{collections::HashMap, env};

///
/// Send a log message on discord
///
pub async fn log(message: &str) {
    let m = "[LOG] ".to_owned() + message;
    msg("4774907", m.as_str()).await;
}

///
/// Send a warning message on discord
///
pub async fn warning(message: &str) {
    let m = "[WAR] ".to_owned() + message;
    msg("16697943", m.as_str()).await;
}

///
/// Send a error message on discord
///
pub async fn error(message: &str) {
    let m = "[ERR] ".to_owned() + message;
    msg("16739179", m.as_str()).await;
}

///
/// Send a success message on discord
///
pub async fn success(message: &str) {
    let m = "[SUC] ".to_owned() + message;
    msg("1954209", m.as_str()).await;
}

///
/// Send a message on discord
///
pub async fn msg(color: &str, message: &str) {
    let e = env::var("PRODUCTION");

    match e {
        Ok(_) => {
            let client = reqwest::Client::new();
            
            let mut embeds: Vec<HashMap<&str, &str>> = Vec::new();
            
            let mut msg: HashMap<&str, &str> = HashMap::new();
            msg.insert("title", message);
            msg.insert("color", color);
            embeds.push(msg);
    
            let mut body = HashMap::new();
            body.insert("embeds", embeds);
    
            match env::var("DISCORD_URL") {
                Ok(url) => {
                    let _response = client.post(url.to_string()).json(&body).send().await;
                },
                Err(_) => {}
            }
        },
        Err(_) => {}
    }
}
