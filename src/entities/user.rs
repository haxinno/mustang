use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct User {
    pub account: String,
    pub admin: bool,
    pub mail: String,
    pub password: String,
    pub token: String, // Token réseaux sociaux
    pub uuid: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct UserLogin {
    pub mail: String,
    pub password: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct UserForm {
    pub account: String,
    pub mail: String,
    pub password: String,
}
