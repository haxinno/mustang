use modules::alarm;
use mysql::{Pool};
use futures::executor::block_on;

use crate::{entities::notification::Notification};

mod modules;
mod entities;
mod providers;

/// Tests all functions
fn main(){

    // For testing connect to a mysql app
    let url = "mysql://admin:mM>>^9H2Nr{4h5xJ@stockinvest.cyq7fkd3zyvy.eu-west-2.rds.amazonaws.com/stockinvest";
    let rds: Pool = Pool::new(url).unwrap();

    // Get all alarms
    let alarms = block_on(alarm::all(rds.get_conn().unwrap()));
    println!("Nb of alarms: {:?}", alarms.len());

    // Get all notifications
    let notifications = block_on(Notification::all(rds.get_conn().unwrap()));
    println!("Nb of alarms: {:?}", notifications.len());

}