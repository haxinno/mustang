
use chrono::{NaiveDateTime, Utc};
use mysql::{PooledConn, Row, params, prelude::Queryable};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize)]
pub struct Alarm {
    pub uuid: String,
    pub key: String,
    pub origin: String,
    pub date: NaiveDateTime,
    pub priorite: i32,
    pub erreur: String,
}

impl Alarm {

    ///
    /// Convert sql datas to entity
    ///
    pub fn convertfrommysql(mut row: Row) -> Alarm {
        Alarm {
            key: row.take::<String,_>("key").unwrap(),
            uuid: row.take::<String,_>("uuid").unwrap(),
            erreur: row.take::<String,_>("erreur").unwrap(),
            date: row.take::<NaiveDateTime,_>("date").unwrap(),
            origin: row.take::<String,_>("origin").unwrap(),
            priorite: row.take::<i32,_>("priorite").unwrap(),
        }
    }
    
    ///
    /// Create
    ///
    pub fn create(key: &str, origin : &str, erreur: &str, priorite: i32) -> Self {
        Self {
            key: key.to_string(),
            date: Utc::now().naive_utc(),
            origin: origin.to_string(),
            priorite: priorite,
            erreur: erreur.to_string(),
            uuid: Uuid::new_v4().to_string(),
        }
    }
    
    ///
    /// Save
    ///
    pub async fn save(&self, mut conn: PooledConn) -> bool {

        let res = conn.exec_drop("INSERT INTO alarme (uuid, `key`, date, priorite, erreur, origin) VALUES (:uuid, :key, :date, :priorite, :erreur, :origin)", params!{
            "uuid" => self.uuid.to_string(),
            "key" => &self.key,
            "priorite" => self.priorite,
            "erreur" => &self.erreur,
            "origin" => &self.origin,
            "date" => &self.date.format("%Y-%m-%d %H:%M:%S").to_string(),
        });
        
        return true;
    }

}