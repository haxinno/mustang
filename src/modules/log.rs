
///
/// Log an action
///
pub async fn log_action(message: &str, file: &str) {
    println!("File: {:?}  |  Message: {:?}", file, message);
}
