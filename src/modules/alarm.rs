use crate::entities::alarm::Alarm;
use crate::entities::notification::Notification;
use crate::providers::discord;
use mysql::Pool;
use mysql::{PooledConn, prelude::Queryable};

///
/// Get all alarms from the database
///
pub async fn all(mut conn: PooledConn) -> Vec<Alarm> {

    // Declare alarms
    let mut alarms: Vec<Alarm> = Vec::new();

    // Do the request
    conn.query_iter("SELECT * FROM stockinvest.alarme ORDER BY date DESC").map(|mut result| {
        loop {
            let r = result.next();
            
            match r {
                Some(e) => {
                    match e {
                        Ok(row) => {
                            alarms.push(Alarm::convertfrommysql(row))
                        },
                        Err(_err) => break
                    }
                },
                None => break
            }

        }
    }).err();

    // Return alarms
    return alarms;

}

///
/// Create + Save
///
pub async fn new(conn: &Pool, key: &str, origin : &str, erreur: &str, priorite: i32, send_discord: bool, send_notification: bool) -> Alarm {

    // Create the alarm
    let newalarm = Alarm::create(&key, &origin, erreur, priorite);

    // Save the alarm
    newalarm.save(conn.get_conn().unwrap()).await;

    // Send notification
    if send_discord {
        discord::error(newalarm.erreur.as_str()).await;
    }

    if send_notification {
        Notification::sends_to_all(conn.get_conn().unwrap(), &"/!\\ Alarme".to_string(), &erreur.to_string()).await;
    }

    newalarm
}
