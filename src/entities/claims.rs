use serde::{Deserialize, Serialize};

///
/// Claims is the visible part of the jwt
///
#[derive(Debug, Serialize, Deserialize)]
pub struct Claims {
    pub exp: usize, // Required (validate_exp defaults to true in validation). Expiration time (as UTC timestamp)
    pub user: String, // If a user is needed
    pub account: String, // If no user is needed
    pub admin: bool
}
